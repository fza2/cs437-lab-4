import json
import logging
import sys
from collections import defaultdict

import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

# Keep track of the max CO2 emission seen so far. This outlives the handler invocations.
max_co2_emission = defaultdict(float)

def lambda_handler(event, context):
    global max_co2_emission

    #TODO1: Get your data
    timestep, vehicle_id, co2_emission = event['timestep'], event['vehicle_id'], event['co2_emission']


    #TODO2: Calculate max CO2 emission
    max_co2_emission[vehicle_id] = max(max_co2_emission[vehicle_id], co2_emission)

    #TODO3: Return the result
    client.publish(
        topic=f"vehicles/vehicle{vehicle_id}/stats",
        payload=json.dumps(
            {
                "timestep": timestep,
                "vehicle_id": vehicle_id,
                "cos_emission": co2_emission,
                "max_co2_emission": max_co2_emission[vehicle_id],
            }
        ),
    )

    return